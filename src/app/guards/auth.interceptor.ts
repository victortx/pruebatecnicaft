import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import {AuthService} from '../services/auth.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private authService: AuthService) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    // Clone the request object
    let newReq = request.clone();
    if ( this.authService.accessToken){
      newReq = request.clone({
        setHeaders: {
          'Access-Control-Allow-Origin' : '*',
          'Access-Control-Allow-Methods' : 'DELETE, POST, GET, OPTIONS',
          'Access-Control-Allow-Headers' : 'Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With',
          'Authorization': `Bearer ${this.authService.accessOnlyToken}`,
        },
      });
    }

    return next.handle(newReq);
  }
}
