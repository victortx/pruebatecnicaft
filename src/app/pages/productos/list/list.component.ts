import { Component, OnInit } from '@angular/core';
import {ProductoService} from '../../../services/producto.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  productos: any[];
  constructor(private productoService: ProductoService, private router: Router) { }

  ngOnInit(): void {
    this.productoService.productsAll().subscribe(response => {
      this.productos = response;
    });
  }

  eliminar(introId){
    this.productoService.delete(introId).subscribe(response => {
      this.router.navigate(['/inicio']);
    });
  }

}
