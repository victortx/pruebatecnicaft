import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { ListComponent } from './list/list.component';
import { NuevoComponent } from './nuevo/nuevo.component';
import {ProductosRoutingModule} from './productos-routing.module';


@NgModule({
  declarations: [
  ListComponent,
  NuevoComponent
  ],
  imports: [
    CommonModule,
    ProductosRoutingModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class ProductosModule { }
