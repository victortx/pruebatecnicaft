import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {ProductoService} from '../../../services/producto.service';

@Component({
  selector: 'app-nuevo',
  templateUrl: './nuevo.component.html',
  styleUrls: ['./nuevo.component.css']
})
export class NuevoComponent implements OnInit {
  public formProducto: FormGroup;
  constructor(private fb: FormBuilder, private route: Router, private productoService: ProductoService) { }

  ngOnInit(): void {
    this.formProducto = this.fb.group({
      name: ['', [Validators.required]],
      price: ['', [Validators.required]],
      stock: ['', [Validators.required]],
      categoryId: [''],
      active: [''],
      category: [''],
    });
  }

  onSubmit() {
    const price = this.formProducto.get('price').value;
    const stock = this.formProducto.get('stock').value;
    const categoryData =  {
      categoryId: 1,
      category: 'Frutas y verduras',
      active: true
    };
    this.formProducto.controls.categoryId.setValue(1);
    this.formProducto.controls.active.setValue(true);
    this.formProducto.controls.category.setValue(categoryData);
    this.formProducto.controls.price.setValue(parseInt(price, 10));
    this.formProducto.controls.stock.setValue(parseInt(stock, 10));
    this.productoService.productSave(this.formProducto.value).subscribe(data => {
      this.route.navigate(['/productos']);
    });
  }

}
