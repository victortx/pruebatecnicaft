import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  submitted: boolean;
  constructor(private formBuilder: FormBuilder, private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]]
    });
  }

  sigIn(): void{
    // set email and password
    const credencials = { username: this.loginForm.get('username').value, password: this.loginForm.get('password').value};
    this.authService.loginUser(credencials).subscribe(
      () => {
        this.submitted = true;
        this.router.navigate(['/inicio']);
      },
      (response) => {
        this.submitted = false;
      });
  }

}
