export interface Credenciales {
  email: string;
  expiration: string;
  id: string;
  rol: string;
  jwt: string;
  username: string;
}
