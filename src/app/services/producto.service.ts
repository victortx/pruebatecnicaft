import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {switchMap} from 'rxjs/operators';
import {Observable, of} from 'rxjs';
import {AuthService} from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class ProductoService {

  constructor(private HttpService: HttpClient, private authService: AuthService) { }

  productsAll(): Observable<any>{
    return this.HttpService.get(`${environment.urlBase}products/all`).pipe(
      switchMap((response: any) => {
        return of(response);
      }),
    );
  }

  productSave(introData): Observable<any> {
    return this.HttpService.post(`${environment.urlBase}products/save`, introData).pipe(
      switchMap((response: any) => {
        return of(response);
      })
    );
  }

  delete(introId): Observable<any> {
    return this.HttpService.delete(`${environment.urlBase}products/delete/${introId}`).pipe(
      switchMap((response: any) => {
        return of(response);
      })
    );
  }

}
