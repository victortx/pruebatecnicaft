import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {Observable, of} from 'rxjs';
import {environment} from '../../environments/environment';
import {switchMap} from 'rxjs/operators';
import {Credenciales} from '../interfaces/credenciales';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private HttpService: HttpClient, private route: Router) { }

  /**
   * Setter & getter for access token
   */
  set accessToken(token: string) {
    localStorage.setItem('accessToken', token);
  }

  get accessToken(): string {
    return localStorage.getItem('accessToken') ?? '';
  }

  get accessOnlyToken(): string {
    const objectToken = JSON.parse(localStorage.getItem('accessToken')) ?? '';
    if (objectToken.hasOwnProperty('token')) {
      return objectToken.token;
    }
    return '';
  }
  /**
   * Sign out
   */
  signOut(): Observable<any> {
    // Remove the access token from the local storage
    localStorage.removeItem('accessToken');
    // Remove id user in local storage
    localStorage.removeItem('username');
    // Set the authenticated flag to false
    // Return the observable
    return of(true);
  }

  /**
   * @param credentials
   */
  loginUser(credentials): Observable<any> {
    return this.HttpService.post(`${environment.urlBase}auth/authenticate`, credentials).pipe(
      switchMap((response: Credenciales) => {
        this.accessToken =  JSON.stringify({token: response.jwt});
        localStorage.setItem('username', response.username);
        return of(response);
      }),
    );
  }

}
